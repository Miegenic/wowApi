package WowApi

import (
	"testing"
)

func TestGetAchievement(t *testing.T) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	_, err := bnet.GetAchievement("2144")
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkGetAchievement(b *testing.B) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		bnet.GetAchievement("2144")
	}
	b.StopTimer()
}
