package WowApi

import (
	"github.com/pquerna/ffjson/ffjson"
	"io/ioutil"
	"net/http"
)

func (b *Bnet) GetCharacterRaces() (Races, error) {
	resp, err := b.CreateReq([]string{"data", "character", "races"}, []string{}, map[string]string{})
	defer resp.Body.Close()
	if err != nil {
		return Races{}, err
	}

	races := Races{}

	err = decodeCharacterRaces(&races, resp)
	return races, err
}

func decodeCharacterRaces(races *Races, resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = ffjson.Unmarshal(body, &races)

	if err != nil {
		return err
	}

	return nil
}