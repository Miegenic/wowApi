package WowApi

import (
	"testing"
	"flag"
)

var bnet_testing_key = flag.String("apikey","","")

func TestGetCharacterRaces(t *testing.T) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	_, err := bnet.GetCharacterRaces()
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkGetCharacterRaces(b *testing.B) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		bnet.GetCharacterRaces()
	}
	b.StopTimer()
}

func BenchmarkDecodeCharacterRaces(b *testing.B) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	resp, err := bnet.CreateReq([]string{"data", "character", "races"}, []string{}, map[string]string{})
	defer resp.Body.Close()

	if err != nil {
		b.Error(err)
	}

	races := Races{}
	b.StartTimer()
	for n := 0; n < b.N; n++ {
		decodeCharacterRaces(&races, resp)
	}
	b.StopTimer()
}