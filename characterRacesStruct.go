package WowApi

type Races struct{
	Races []Race `json:"races"`
}

type Race struct {
	Id uint16 `json:"id"`
	Mask uint `json:"mask"`
	Side string `json:"side"`
	Name string `json:"name"`
}