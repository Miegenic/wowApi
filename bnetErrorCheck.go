package WowApi

import (
	"net/http"
	"github.com/pquerna/ffjson/ffjson"
	"io/ioutil"
	"errors"
)

type ForbiddenError struct {
	Code   int `json:"code"`
	Type   string `json:"type"`
	Detail string `json:"detail"`
}

type StatusError struct {
	Status string `json:"status"`
	Reason string `json:"reason"`
}

func checkForbiddenError(resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	forbiddenError := ForbiddenError{}

	err = ffjson.Unmarshal(body, &forbiddenError)

	if err != nil {
		return err
	}

	return errors.New(forbiddenError.Detail)
}

func checkStatusError(resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	statusError := StatusError{}
	err = ffjson.Unmarshal(body, &statusError)

	if err != nil {
		return err
	}

	return errors.New(statusError.Status)
}
