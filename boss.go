package WowApi

import (
	"io/ioutil"
	"net/http"
	"github.com/pquerna/ffjson/ffjson"
)

func (b *Bnet) GetBossMasterList() (BossMaster, error) {
	resp, err := b.CreateReq([]string{"boss/"}, []string{}, map[string]string{})
    
	defer resp.Body.Close()

	if err != nil {
		return BossMaster{}, err
	}

	bossMaster := BossMaster{}

	err = decodeBossMaster(&bossMaster, resp)


	return bossMaster, err
}

func (b *Bnet) GetBoss(id string) (Boss, error) {
    resp, err := b.CreateReq([]string{"boss"}, []string{id}, map[string]string{})
    
	defer resp.Body.Close()
    
	if err != nil {
		return Boss{}, err
	}

	boss := Boss{}

	err = decodeBoss(&boss, resp)


	return boss, err
}

func decodeBossMaster(bossMaster *BossMaster, resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)
    
	if err != nil {
		return err
	}
    
	err = ffjson.Unmarshal(body, &bossMaster)

	return err
}

func decodeBoss(boss *Boss, resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)
    
	if err != nil {
		return err
	}
    
	err = ffjson.Unmarshal(body, &boss)

	return err
}
