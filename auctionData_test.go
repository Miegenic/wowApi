package WowApi

import (
	"testing"
)

func TestGetAuctionDataStatus(t *testing.T) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	_, err := bnet.GetAuctionDataStatus("outland")
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkGetAuctionDataStatus(b *testing.B) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		bnet.GetAuctionDataStatus("outland")
	}
	b.StopTimer()
}

func TestGetAuctionData(t *testing.T) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	auction, err := bnet.GetAuctionDataStatus("outland")
	_, err = auction.Files[0].GetData()
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkGetAuctionData(b *testing.B) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	auction, _ := bnet.GetAuctionDataStatus("outland")

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		auction.Files[0].GetData()
	}
	b.StopTimer()
}
