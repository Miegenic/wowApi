package WowApi

import (
	"testing"
	"errors"
)

func TestCreateApi(t *testing.T) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	bnetFromStruct := Bnet{
		Key: "asdasdasdasdasdasdasdasdasdasdas",
		Region: "eu",
		Lang: "en_GB",
	}

	if bnet.Key != bnetFromStruct.Key {
		t.Error("CreateApi (API Key) error")
	}

	if bnet.Region != bnetFromStruct.Region {
		t.Error("CreateApi (Region) error")
	}

	if bnet.Lang != bnetFromStruct.Lang {
		t.Error("CreateApi (Lang) error")
	}

}

func TestCreateApiTrue(t *testing.T) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")

	err := bnet.CheckKey()
	if err != nil {
		t.Error(err)
	}

	err = bnet.CheckLang()
	if err != nil {
		t.Error(err)
	}

	err = bnet.CheckRegion()
	if err != nil {
		t.Error(err)
	}
}

func TestCreateApiFalse(t *testing.T) {
	bnet := CreateApi("123", "ay", "am")

	err := bnet.CheckKey()
	if err == nil {
		t.Error("CheckKey not working correctly.")
	}

	err = bnet.CheckLang()
	if err == nil {
		t.Error("CheckLang not working correctly.")
	}

	err = bnet.CheckRegion()
	if err == nil {
		t.Error("CheckRegion not working correctly.")
	}
}

func TestCreateApiGuild(t *testing.T) {
	guild := GuildConfig{
		Name: "Forgotten Legion",
		Realm: "Outland",
	}
	bnet := CreateApiGuild("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB", guild)

	bnetFromStruct := Bnet{
		Key: "asdasdasdasdasdasdasdasdasdasdas",
		Region: "eu",
		Lang: "en_GB",
		Guild: GuildConfig{
			Name: "Forgotten Legion",
			Realm: "Outland",
		},
	}

	if bnet.Key != bnetFromStruct.Key {
		t.Error("CreateApi (API Key) error")
	}

	if bnet.Region != bnetFromStruct.Region {
		t.Error("CreateApi (Region) error")
	}

	if bnet.Lang != bnetFromStruct.Lang {
		t.Error("CreateApi (Lang) error")
	}
}

func TestCreateApiGuildTrue(t *testing.T) {
	guild := GuildConfig{
		Name: "Forgotten Legion",
		Realm: "Outland",
	}
	bnet := CreateApiGuild("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB", guild)

	err := bnet.CheckKey()
	if err != nil {
		t.Error(err)
	}

	err = bnet.CheckLang()
	if err != nil {
		t.Error(err)
	}

	err = bnet.CheckRegion()
	if err != nil {
		t.Error(err)
	}
}

func TestCreateApiGuildFalse(t *testing.T) {
	guild := GuildConfig{
		Name: "Forgotten Legion",
		Realm: "Outland",
	}
	bnet := CreateApiGuild("123", "ay", "am", guild)

	err := bnet.CheckKey()
	if err == nil {
		t.Error("CheckKey not working correctly.")
	}

	err = bnet.CheckLang()
	if err == nil {
		t.Error("CheckLang not working correctly.")
	}

	err = bnet.CheckRegion()
	if err == nil {
		t.Error("CheckRegion not working correctly.")
	}
}

func TestCreateApiFull(t *testing.T) {
	bnet, _ := CreateApiFull("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB", "Forgotten Legion", "Outland")

	bnetFromStruct := Bnet{
		Key: "asdasdasdasdasdasdasdasdasdasdas",
		Region: "eu",
		Lang: "en_GB",
		Guild: GuildConfig{
			Name: "Forgotten Legion",
			Realm: "Outland",
		},
	}

	if bnet.Key != bnetFromStruct.Key {
		t.Error("CreateApi (API Key) error")
	}

	if bnet.Region != bnetFromStruct.Region {
		t.Error("CreateApi (Region) error")
	}

	if bnet.Lang != bnetFromStruct.Lang {
		t.Error("CreateApi (Lang) error")
	}
}

func TestCreateApiFullTrue(t *testing.T) {
	bnet, _ := CreateApiFull("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB", "Forgotten Legion", "Outland")

	err := bnet.CheckKey()
	if err != nil {
		t.Error(err)
	}

	err = bnet.CheckLang()
	if err != nil {
		t.Error(err)
	}

	err = bnet.CheckRegion()
	if err != nil {
		t.Error(err)
	}
}

func TestCreateApiFullFalse(t *testing.T) {
	bnet, _ := CreateApiFull("123", "ay", "am", "Forgotten Legion", "Outland")

	err := bnet.CheckKey()
	if err == nil {
		t.Error("CheckKey not working correctly.")
	}

	err = bnet.CheckLang()
	if err == nil {
		t.Error("CheckLang not working correctly.")
	}

	err = bnet.CheckRegion()
	if err == nil {
		t.Error("CheckRegion not working correctly.")
	}
}

// Create Req

func TestBnet_CreateReq(t *testing.T) {
	bnet, _ := CreateApiFull("123", "eu", "en_GB", "Forgotten Legion", "Outland")
	_, err := bnet.CreateReq([]string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})

	if err == nil {
		t.Error(errors.New("Bnet.CreateReq() error"))
	}
}


// Benchmarks

func BenchmarkCreateApi(b *testing.B) {
	for n := 0; n < b.N; n++ {
		CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	}
}

func BenchmarkCreateApiGuild(b *testing.B) {
	guild := GuildConfig{
		Name: "Forgotten Legion",
		Realm: "Outland",
	}
	for n := 0; n < b.N; n++ {
		CreateApiGuild("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB", guild)
	}
}

func BenchmarkCreateApiFull(b *testing.B) {
	for n := 0; n < b.N; n++ {
		CreateApiFull("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB", "Forgotten Legion", "Outland")
	}
}