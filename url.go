package WowApi


type Url struct {
	Bnet      *Bnet
	Base      string
	Endpoints string
	Ids       string
	Args      string
}

func CreateUrl(bnet Bnet, endpoints []string, ids []string, args map[string]string) *Url {
	url := Url{}

	url.Bnet = &bnet

	url.Base = "https://" + bnet.Region + ".api.battle.net/wow"
	for _, v := range endpoints {
		url.Endpoints += "/" + v
	}
	for _, v := range ids {
		url.Ids += "/" + v
	}
	url.Args = "?"
	for k, v := range args {
		url.Args += k + "=" + v + "&"
	}
	url.Args += "locale=" + bnet.Lang + "&apikey=" + bnet.Key

	return &url
}

func (u *Url) GenUrl() string {
	return u.Base + u.Endpoints + u.Ids + u.Args
}

func (u *Url) ChangeEndpoints(endpoints []string) {
	u.Endpoints = ""
	for _, v := range endpoints {
		u.Endpoints += "/" + v
	}
}

func (u *Url) ChangeIds(ids []string) {
	u.Ids = ""
	for _, v := range ids {
		u.Ids += "/" + v
	}
}

func (u *Url) ChangeArgs(args map[string]string) {
	u.Args = "?"
	for k, v := range args {
		u.Args += k + "=" + v + "&"
	}
	u.Args += "locale=" + u.Bnet.Lang + "&apikey=" + u.Bnet.Key
}