package WowApi

import (
	"github.com/pquerna/ffjson/ffjson"
	"io/ioutil"
	"net/http"
)

func (b *Bnet) GetAchievement(id string) (Achievement, error) {
	resp, err := b.CreateReq([]string{"achievement"}, []string{id}, map[string]string{})
	defer resp.Body.Close()
    
	if err != nil {
		return Achievement{}, err
	}

	achievement := Achievement{}

	err = decodeAchievement(&achievement, resp)
	return achievement, err
}

func decodeAchievement(achi *Achievement, resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = ffjson.Unmarshal(body, &achi)

	return err
}