package WowApi

import "testing"

func TestCreateUrl(t *testing.T) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	url := CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})
	urlFromStruct := Url{
		Base: "https://eu.api.battle.net/wow",
		Endpoints: "/character",
		Ids: "/outland/aragaroln",
		Args: "?fields=mounts&locale=en_GB&apikey=asdasdasdasdasdasdasdasdasdasdas",
	}

	if url.Base != urlFromStruct.Base {
		t.Error("CreateUrl (Base) error")
	}

	if url.Endpoints != urlFromStruct.Endpoints {
		t.Error("CreateUrl (Endpoints) error")
	}

	if url.Ids != urlFromStruct.Ids {
		t.Error("CreateUrl (Ids) error")
	}

	if url.Args != urlFromStruct.Args {
		t.Error("CreateUrl (Args) error")
	}
}

func TestUrl_GenUrl(t *testing.T) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	url := CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})

	genUrl := url.GenUrl()
	genUrlCorrect := "https://eu.api.battle.net/wow/character/outland/aragaroln?fields=mounts&locale=en_GB&apikey=asdasdasdasdasdasdasdasdasdasdas"

	if genUrl != genUrlCorrect {
		t.Error("Url.GenUrl() method error")
	}

	url.ChangeArgs(map[string]string{"fields":"pets"})

	genUrl = url.GenUrl()
	genUrlCorrect = "https://eu.api.battle.net/wow/character/outland/aragaroln?fields=pets&locale=en_GB&apikey=asdasdasdasdasdasdasdasdasdasdas"

	if genUrl != genUrlCorrect {
		t.Error("Url.GenUrl() method error")
	}
}

func TestUrl_ChangeFunctions(t *testing.T) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	url := CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})
	urlFromStruct := Url{
		Base: "https://eu.api.battle.net/wow",
		Endpoints: "/character",
		Ids: "/outland/aragaroln",
		Args: "?fields=mounts&locale=en_GB&apikey=asdasdasdasdasdasdasdasdasdasdas",
	}

	if url.Base != urlFromStruct.Base {
		t.Error("CreateUrl (Base) error")
	}

	if url.Endpoints != urlFromStruct.Endpoints {
		t.Error("CreateUrl (Endpoints) error")
	}

	if url.Ids != urlFromStruct.Ids {
		t.Error("CreateUrl (Ids) error")
	}

	if url.Args != urlFromStruct.Args {
		t.Error("CreateUrl (Args) error")
	}

	url.ChangeEndpoints([]string{"achievement"})
	url.ChangeIds([]string{"2144"})
	url.ChangeArgs(map[string]string{})

	urlFromStruct = Url{
		Base: "https://eu.api.battle.net/wow",
		Endpoints: "/achievement",
		Ids: "/2144",
		Args: "?locale=en_GB&apikey=asdasdasdasdasdasdasdasdasdasdas",
	}

	if url.Base != urlFromStruct.Base {
		t.Error("CreateUrl (Base) error")
	}

	if url.Endpoints != urlFromStruct.Endpoints {
		t.Error("Url.ChangeEndpoints() error")
	}

	if url.Ids != urlFromStruct.Ids {
		t.Error("Url.ChangeIds() error")
	}

	if url.Args != urlFromStruct.Args {
		t.Error("Url.ChangeArgs() error")
	}
}

func BenchmarkCreateUrl(b *testing.B) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	for n := 0; n < b.N; n++ {
		CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})
	}
}

func BenchmarkUrl_GenUrl(b *testing.B) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	url := CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})
	for n := 0; n < b.N; n++ {
		url.GenUrl()
	}
}


func BenchmarkUrl_ChangeEndpoints(b *testing.B) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	url := CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})
	for n := 0; n < b.N; n++ {
		url.ChangeEndpoints([]string{"123", "123"})
	}
}

func BenchmarkUrl_ChangeIds(b *testing.B) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	url := CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})
	for n := 0; n < b.N; n++ {
		url.ChangeIds([]string{"123", "123"})
	}
}

func Benchmark_ChangeArgs(b *testing.B) {
	bnet := CreateApi("asdasdasdasdasdasdasdasdasdasdas", "eu", "en_GB")
	url := CreateUrl(*bnet, []string{"character"}, []string{"outland", "aragaroln"}, map[string]string{"fields":"mounts"})
	for n := 0; n < b.N; n++ {
		url.ChangeArgs(map[string]string{"fields":"achievements,appearance,feed,guild,items,mounts,pets,progression,pvp,quests,reputation,statistics,stats,talents,titles,audit"})
	}
}