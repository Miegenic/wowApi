package WowApi

import (
	"testing"
)

func TestGetBossMasterList(t *testing.T) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	_, err := bnet.GetBossMasterList()
	if err != nil {
		t.Error(err)
	}
}

func TestGetBoss(t *testing.T) {
    bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
    _, err := bnet.GetBoss("24723")
    if err != nil {
        t.Error(err)
    }
}