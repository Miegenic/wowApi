package WowApi

type ChallengeMode struct {
	Challenge []struct {
		Groups []struct {
			Date        string `json:"date"`
			Faction     string `json:"faction"`
			IsRecurring bool   `json:"isRecurring"`
			Medal       string `json:"medal"`
			Members     []struct {
				Character struct {
					AchievementPoints int    `json:"achievementPoints"`
					Battlegroup       string `json:"battlegroup"`
					Class             int    `json:"class"`
					Gender            int    `json:"gender"`
					Guild             string `json:"guild"`
					GuildRealm        string `json:"guildRealm"`
					LastModified      int    `json:"lastModified"`
					Level             int    `json:"level"`
					Name              string `json:"name"`
					Race              int    `json:"race"`
					Realm             string `json:"realm"`
					Spec              struct {
						BackgroundImage string `json:"backgroundImage"`
						Description     string `json:"description"`
						Icon            string `json:"icon"`
						Name            string `json:"name"`
						Order           int    `json:"order"`
						Role            string `json:"role"`
					} `json:"spec"`
					Thumbnail string `json:"thumbnail"`
				} `json:"character"`
				Spec struct {
					BackgroundImage string `json:"backgroundImage"`
					Description     string `json:"description"`
					Icon            string `json:"icon"`
					Name            string `json:"name"`
					Order           int    `json:"order"`
					Role            string `json:"role"`
				} `json:"spec"`
			} `json:"members"`
			Ranking int `json:"ranking"`
			Time    struct {
				Hours        int  `json:"hours"`
				IsPositive   bool `json:"isPositive"`
				Milliseconds int  `json:"milliseconds"`
				Minutes      int  `json:"minutes"`
				Seconds      int  `json:"seconds"`
				Time         int  `json:"time"`
			} `json:"time"`
		} `json:"groups"`
		Map struct {
			BronzeCriteria struct {
				Hours        int  `json:"hours"`
				IsPositive   bool `json:"isPositive"`
				Milliseconds int  `json:"milliseconds"`
				Minutes      int  `json:"minutes"`
				Seconds      int  `json:"seconds"`
				Time         int  `json:"time"`
			} `json:"bronzeCriteria"`
			GoldCriteria struct {
				Hours        int  `json:"hours"`
				IsPositive   bool `json:"isPositive"`
				Milliseconds int  `json:"milliseconds"`
				Minutes      int  `json:"minutes"`
				Seconds      int  `json:"seconds"`
				Time         int  `json:"time"`
			} `json:"goldCriteria"`
			HasChallengeMode bool   `json:"hasChallengeMode"`
			ID               int    `json:"id"`
			Name             string `json:"name"`
			SilverCriteria   struct {
				Hours        int  `json:"hours"`
				IsPositive   bool `json:"isPositive"`
				Milliseconds int  `json:"milliseconds"`
				Minutes      int  `json:"minutes"`
				Seconds      int  `json:"seconds"`
				Time         int  `json:"time"`
			} `json:"silverCriteria"`
			Slug string `json:"slug"`
		} `json:"map"`
		Realm struct {
			Battlegroup     string   `json:"battlegroup"`
			ConnectedRealms []string `json:"connected_realms"`
			Locale          string   `json:"locale"`
			Name            string   `json:"name"`
			Slug            string   `json:"slug"`
			Timezone        string   `json:"timezone"`
		} `json:"realm"`
	} `json:"challenge"`
}

type Challenge struct {
	Realm  ChallengeRealm    `json:"realm"`
	Map    ChallengeMap      `json:"map"`
	Groups []ChallengeGroups `json:"groups"`
}

type ChallengeRealm struct {
	Name             string   `json:"name"`
	Slug             string   `json:"slug"`
	Battlegroup      string   `json:"battlegroup"`
	Locale           string   `json:"locale"`
	Timezone         string   `json:"timezone"`
	Connected_realms []string `json:"connected_realms"`
}

type ChallengeMap struct {
	Id               int          `json:"id"`
	Name             string       `json:"name"`
	Slug             string       `json:"slug"`
	HasChallengeMode bool         `json:"hasChallengeMode"`
	BronzeCriteria   CriteriaTime `json:"bronzeCriteria"`
	SilverCriteria   CriteriaTime `json:"silverCriteria"`
	GoldCriteria     CriteriaTime `json:"goldCriteria"`
}

type ChallengeGroups struct {
	Ranking     int              `json:"ranking"`
	Time        CriteriaTime     `json:"time"`
	Date        string           `json:"date"`
	Medal       string           `json:"medal"`
	Faction     string           `json:"faction"`
	IsRecurring bool             `json:"isRecurring"`
	Members     []CriteriaMember `json:"members'`
}

type CriteriaTime struct {
	Time         int  `json:"time"`
	Hours        int  `json:"hours"`
	Minutes      int  `json:"minutes"`
	Seconds      int  `json:"seconds"`
	Milliseconds int  `json:"milliseconds"`
	IsPositive   bool `json:"isPositive"`
}

type CriteriaMember struct {
	Character struct {
		Name              string             `json:"name"`
		Realm             string             `json:"realm"`
		Battlegroup       string             `json:"battlegroup"`
		Class             int                `json:"class"`
		Race              int                `json:"race"`
		Level             int                `json:"level"`
		AchievementPoints int                `json:"achievementPoints"`
		Thumbnail         string             `json:"thumbnail"`
		Spec              CriteriaMemberSpec `json:"spec"`
	} `json:"character"`
	Spec CriteriaMemberSpec `json:"spec"`
}

type CriteriaMemberSpec struct {
	Name            string `json:"name"`
	Role            string `json:"role"`
	BackgroundImage string `json:"backgroundImage"`
	Icon            string `json:"icon"`
	Description     string `json:"description"`
	Order           int    `json:"order"`
}
