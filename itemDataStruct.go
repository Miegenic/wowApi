package WowApi

type Item struct {
	Id                     int          `json:"id"`
	DisenchantingSkillRank int          `json:"disenchantingSkillRank"`
	Description            string       `json:"description"`
	Name                   string       `json:"name"`
	Icon                   string       `json:"icon"`
	Stackable              int          `json:"stackable"`
	ItemBind               int          `json:"itemBind"`
	BonusStats             []BonusStats `json:"bonusStats"`
	BuyPrice               int          `json:"buyPrice"`
	ItemClass              int          `json:"itemClass"`
	ItemSubClass           int          `json:"itemSubClass"`
	ContainerSlots         int          `json:"containerSlots"`
	InventoryType          int          `json:"inventoryType"`
	Equippable             bool         `json:"equippable"`
	ItemLevel              int          `json:"itemLevel"`
	MaxCount               int          `json:"maxCount"`
	MaxDurability          int          `json:"maxDurability"`
	MinFactionId           int          `json:"minFactionId"`
	MinReputation          int          `json:"minReputation"`
	Quality                int          `json:"quality"`
	SellPrice              int          `json:"sellPrice"`
	RequiredSkill          int          `json:"requiredSkill"`
	RequiredLevel          int          `json:"requiredLevel"`
	RequiredSkillRank      int          `json:"requiredSkillRank"`
	ItemSource             ItemSource   `json:"itemSource"`
	BaseArmor              int          `json:"baseArmor"`
	HasSockets             bool         `json:"hasSockets"`
	IsAuctionable          bool         `json:"isAuctionable"`
	Armor                  int          `json:"armor"`
	DisplayInfoId          int          `json:"displayInfoId"`
	NameDescription        string       `json:"nameDescription"`
	NameDescriptionColor   string       `json:"nameDescriptionColor"`
	Upgradable             bool         `json:"upgradable"`
	HeroicTooltip          bool         `json:"heroicTooltip"`
	Context                string       `json:"context"`
}

type BonusStats struct {
	Stat   int `json:"stat"`
	Amount int `json:"amount"`
}

type ItemWeaponInfo struct {
	Damage      ItemWeaponInfoDamage `json:"damage"`
	WeaponSpeed float64              `json:"weaponSpeed"`
	Dps         float64              `json:"dps"`
}

type ItemWeaponInfoDamage struct {
	Min      int     `json:"min"`
	Max      int     `json:"max"`
	ExactMin float64 `json:"exactMin"`
	ExactMax float64 `json:"exactMax"`
}

type ItemSource struct {
	SourceId   int    `json:"sourceId"`
	SourceType string `json:"sourceType"`
}
