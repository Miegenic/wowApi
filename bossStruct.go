package WowApi

type BossMaster struct {
	Bosses []Boss `json:"bosses"`
}

type Boss struct {
	Id                    int        `json:"id"`
	Name                  string     `json:"name"`
	UrlSlug               string     `json:"urlSlug"`
	Description           string     `json:"description"`
	ZoneId                int        `json:"zoneId"`
	AvailableInNormalMode bool       `json:"availableInNormalMode"`
	AvailableInHeroicMode bool       `json:"availableInHeroicMode"`
	Health                int        `json:"health"`
	HeroicHealth          int        `json:"heroicHealth"`
	Level                 int        `json:"level"`
	HeroicLevel           int        `json:"heroicLevel"`
	JournalId             int        `json:"journalId"`
	Npcs                  []BossNpcs `json:"npcs"`
}

type BossNpcs struct {
	Id      int    `json:"id"`
	Name    string `json:"name"`
	UrlSlug string `json:"urlSlug"`
}