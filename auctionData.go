package WowApi

import (
	"github.com/pquerna/ffjson/ffjson"
	"io/ioutil"
	"net/http"
	"sync"
	"strconv"
)

func (b *Bnet) GetAuctionDataStatus(realm string) (AuctionDataStatus, error) {
	resp, err := b.CreateReq([]string{"auction", "data"}, []string{realm}, map[string]string{})
	defer resp.Body.Close()
	if err != nil {
		return AuctionDataStatus{}, err
	}

	auctionDataStatus := AuctionDataStatus{}

	err = decodeAuctionDataStatus(&auctionDataStatus, resp)
	return auctionDataStatus, nil
}

func (a *AuctionFiles) GetData() (AuctionData, error) {
	resp, err := http.Get(a.Url)
	defer resp.Body.Close()

	if err != nil {
		return AuctionData{}, err
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return AuctionData{}, err
	}

	auctionData := AuctionData{}

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		err = ffjson.Unmarshal(body, &auctionData)
	}()

	wg.Wait()

	if err != nil {
		return AuctionData{}, err
	}

	return auctionData, nil
}

func (a *AuctionDataAuction) Expand(bnet *Bnet) (AuctionDataAuctionExp, error) {
	item, err := bnet.GetItem(strconv.Itoa(a.Item))

	if err != nil {
		return AuctionDataAuctionExp{}, err
	}

	auctionDataAuctionExp := AuctionDataAuctionExp{
		Auction: *a,
		Item: item,
	}

	return auctionDataAuctionExp, nil
}

func decodeAuctionDataStatus(auctionDataStatus *AuctionDataStatus, resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = ffjson.Unmarshal(body, &auctionDataStatus)

	if err != nil {
		return err
	}

	return nil
}
