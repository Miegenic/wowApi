package WowApi

import (
	"testing"
)

func TestGetItem(t *testing.T) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")
	_, err := bnet.GetItem("18803")
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkGetItem(b *testing.B) {
	bnet := CreateApi(*bnet_testing_key, "eu", "en_GB")

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		bnet.GetItem("18803")
	}
	b.StopTimer()
}