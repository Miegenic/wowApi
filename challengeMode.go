package WowApi

import (
	"io/ioutil"
	"net/http"
	"github.com/pquerna/ffjson/ffjson"
)


func (b *Bnet) GetRealmLeaderboard(realm string) (ChallengeMode, error) {
    	resp, err := b.CreateReq([]string{"boss/"}, []string{}, map[string]string{})
    
	defer resp.Body.Close()

	if err != nil {
		return ChallengeMode{}, err
	}

	challengeMode := ChallengeMode{}

	err = decodeRealmLeaderboard(&challengeMode, resp)

	return challengeMode, err
}

func decodeRealmLeaderboard(challengeMode *ChallengeMode, resp *http.Response) error {
    body, err := ioutil.ReadAll(resp.Body)
    
	if err != nil {
		return err
	}
    
	err = ffjson.Unmarshal(body, &challengeMode)

	return err
}