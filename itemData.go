package WowApi

import (
	"github.com/pquerna/ffjson/ffjson"
	"io/ioutil"
	"net/http"
)

func (b *Bnet) GetItem(id string) (Item, error) {
	resp, err := b.CreateReq([]string{"item"}, []string{id}, map[string]string{})
	defer resp.Body.Close()
	if err != nil {
		return Item{}, err
	}

	item := Item{}

	err = decodeItem(&item, resp)
	return item, err
}

func decodeItem(item *Item, resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = ffjson.Unmarshal(body, &item)

	if err != nil {
		return err
	}

	return nil
}