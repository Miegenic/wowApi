package WowApi

type Achievement struct {
	Id          int `json:"id"`
	Title       string `json:"title"`
	Points      int `json:"points"`
	Description string `json:"description"`
	Reward      string `json:"reward"`
	Icon        string `json:"icon"`
	Criteria    []AchievementCriteria `json:"criteria"`
	RewardItems []AchievementRewardItems `json:"rewardItems"`
	AccountWide bool `json:"accountWide"`
	FactionId   int `json:"factionId"`
}

type AchievementRewardItems struct {
	Id            int `json:"id"`
	Name          string `json:"name"`
	Icon          string `json:"icon"`
	Quality       int `json:"quality"`
	ItemLevel     int `json:"itemLevel"`
	TooltipParams ToolTipParams `json:"tooltipParams"`
	Armor         int `json:"armor"`
	Context       string `json:"context"`
}

type ToolTipParams struct {
	TimewalkerLevel int `json:"timewalkerLevel"`
}

type AchievementCriteria struct {
	Id          int `json:"id"`
	Description string `json:"description"`
	OrderIndex  int `json:"orderIndex"`
	Max         int `json:"max"`
}