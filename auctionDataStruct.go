package WowApi

type AuctionDataStatus struct {
	Files []AuctionFiles `json:"files"`
}

type AuctionFiles struct {
	Url          string `json:"url"`
	LastModified int    `json:"lastModified"`
}

type AuctionData struct {
	Realms   []AuctionDataRealms  `json:"realms"`
	Auctions []AuctionDataAuction `json:"auctions"`
}

type AuctionDataRealms struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
}

type AuctionDataAuction struct {
	Auc        int                   `json:"auc"`
	Item       int                   `json:"item"`
	Owner      string                `json:"owner"`
	OwnerRealm string                `json:"ownerRealm"`
	Bid        int                   `json:"bid"`
	Buyout     int                   `json:"buyout"`
	Quantity   int                   `json:"quantity"`
	TimeLeft   string                `json:"timeLeft"`
	Rand       int                   `json:"rand"`
	Seed       int                   `json:"seed"`
	Context    int                   `json:"context"`
	Modifiers  []AuctionDataModifier `json:"modifiers"`
}
type AuctionDataModifier struct {
	Type  int `json:"type"`
	Value int `json:"value"`
}

type AuctionDataExp struct {
	Realms   []AuctionDataRealms
	Auctions []AuctionDataAuctionExp
}

type AuctionDataAuctionExp struct {
	Auction AuctionDataAuction
	Item    Item
}