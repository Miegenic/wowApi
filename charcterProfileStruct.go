package WowApi

type CharacterProfile struct {
	LastModified        int `json:"lastModified"`
	Name                string `json:"name"`
	Realm               string `json:"realm"`
	Battlegroup         string `json:"battlegroup"`
	Class               int `json:"class"`
	Race                int `json:"race"`
	Gender              int `json:"gender"`
	Level               int `json:"level"`
	AchievementPoints   int `json:"achievementPoints"`
	Thumbnail           string `json:"thumbnail"`
	CalcClass           string `json:"calcClass"`
	Faction             int `json:"faction"`
	TotalHonorableKills int `json:"totalHonorableKills"`
	Achievements        ProfileAchievements `json:"achievements"`
	Appearance          ProfileAppearance `json:"appearance"`
	Feed                []ProfileFeed `json:"feed"`
	Guild               ProfileGuild `json:"guild"`
	HunterPets          ProfileHunterPets `json:"hunterPets"`
	Items               ProfileItems `json:"items"`
	Mounts              ProfileMounts `json:"mounts"`
	Pets                ProfilePets `json:"pets"`
	PetSlots            []ProfilePetSlots `json:"petSlots"`
	Progression         ProfileProgression `json:"progression"`
	Pvp                 ProfilePvp `json:"pvp"`
	Quests              []int `json:"quests"`
	Reputation          []ProfileReputation `json:"reputation"`
	Statistics          ProfileStatistics `json:"statistics"`
	Stats               ProfileStats `json:"stats"`
	Talents             []ProfileTalents `json:"talents"`
	Titles              []ProfileTitles `json:"titles"`
	Audit               ProfileAudit `json:"audit"`
}

type ProfileAchievements struct {
	AchievementsCompleted          []int `json:"achievementsCompleted"`
	AchievementsCompletedTimestamp []int `json:"achievementsCompletedTimestamp"`
	Criteria                       []int `json:"criteria"`
	CriteriaQuantity               []int `json:"criteriaQuantity"`
	CriteriaTimestamp              []int `json:"criteriaTimestamp"`
	CriteriaCreated                []int `json:"criteriaCreated"`
}

type ProfileAppearance struct {
	FaceVariation    int `json:"faceVariation"`
	SkinColor        int `json:"skinColor"`
	HairVariation    int `json:"hairVariation"`
	HairColor        int `json:"hairColor"`
	FeatureVariation int `json:"featureVariation"`
	ShowHelm         bool `json:"showHelm"`
	ShowCloak        bool `json:"showCloak"`
}

type ProfileFeed struct {
	Type           string `json:"type"`
	Timestamp      int `json:"timestamp"`
	Achievement    Achievement `json:"achievement"`
	FeatOfStrength bool `json:"featOfStrength"`
	Criteria       AchievementCriteria `json:"criteria"`
	Quantity       int `json:"quantity"`
	Name           string `json:"name"`

	ItemId         int `json:"itemId"`
	Context        string `json:"context"`
	BonusLists     []int `json:"bonusLists"`
}

type ProfileGuild struct {
	Name              string `json:"name"`
	Realm             string `json:"realm"`
	Battlegroup       string `json:"battlegroup"`
	Members           int `json:"members"`
	AchievementPoints int `json:"achievementPoints"`
}

type ProfileGuildEmblem struct {
	Icon            int `json:"icon"`
	IconColor       string `json:"iconColor"`
	Border          int `json:"border"`
	BorderColor     string `json:"borderColor"`
	BackgroundColor string `json:"backgroundColor"`
}

type ProfileHunterPets struct {
	Name       string `json:"name"`
	Creature   int `json:"creature"`
	Slot       int `json:"slot"`
	CalcSpec   string `json:"calcSpec"`
	FamilyId   int `json:"familyId"`
	FamilyName string `json:"familyName"`
	Spec       ProfileHunterPetsSpec `json:"spec"`
}

type ProfileHunterPetsSpec struct {
	Name            string `json:"name"`
	Role            string `json:"role"`
	BackgroundImage string `json:"backgroundImage"`
	Icon            string `json:"string"`
	Description     string `json:"description"`
	Order           int `json:"order"`
}

type ProfileItems struct {
	AverageItemLevel         int `json:"averageItemLevel"`
	AverageItemLevelEquipped int `json:"averageItemLevelEquipped"`
	Head                     Item `json:"head"`
	Neck                     Item `json:"neck"`
	Shoulder                 Item `json:"shoulder"`
	Back                     Item `json:"back"`
	Chest                    Item `json:"chest"`
	Wrist                    Item `json:"wrist"`
	Hands                    Item `json:"hands"`
	Waist                    Item `json:"waist"`
	Legs                     Item `json:"legs"`
	Feed                     Item `json:"feed"`
	Finger1                  Item `json:"finger1"`
	Finger2                  Item `json:"finger2"`
	Trinket1                 Item `json:"trinket1"`
	Trinket2                 Item `json:"trinket2"`
	MainHand                 Item `json:"mainHand"`
	OffHand                  Item `json:"offHand"`
}

type ProfileMounts struct {
	NumCollected    int `json:"numCollected"`
	NumNotCollected int `json:"numNotCollected"`
	Collected       []ProfileMountsCollected `json:"collected"`
}

type ProfileMountsCollected struct {
	Name       string `json:"name"`
	SpellId    int `json:"spellId"`
	CreatureId int `json:"creatureId"`
	ItemId     int `json:"itemId"`
	QualityId  int `json:"qualityId"`
	Icon       string `json:"icon"`
	IsGround   bool `json:"isGround"`
	IsFlying   bool `json:"isFlying"`
	IsAquatic  bool `json:"isAquatic"`
	isJumping  bool `json:"isJumping"`
}

type ProfilePets struct {
	NumCollected    int `json:"numCollected"`
	NumNotCollected int `json:"numNotCollected"`
	Collected       []ProfilePetsCollected `json:"collected"`
}

type ProfilePetsCollected struct {
	Name                        string `json:"name"`
	SpellId                     int `json:"spellId"`
	CreatureId                  int `json:"creatureId"`
	ItemId                      int `json:"itemId"`
	QualityId                   int `json:"qualityId"`
	Icon                        string `json:"icon"`
	Stats                       ProfilePetsCollectedStats `json:"stats"`
	BattlePetGuid               string `json:"battlePetGuid"`
	IsFavorite                  bool `json:"isFavorite"`
	IsFirstAbilitySlotSelected  bool `json:"isFirstAbilitySlotSelected"`
	IsSecondAbilitySlotSelected bool `json:"isSecondAbilitySlotSelected"`
	IsThirdAbilitySlotSelected  bool `json:"isThirdAbilitySlotSelected"`
	CreatureName                string `json:"creatureName"`
	CanBattle                   bool `json:"canBattle"`
}

type ProfilePetsCollectedStats struct {
	SpeciesId    int `json:"speciesId"`
	BreedId      int `json:"breedId"`
	PetQualityId int `json:"petQualityId"`
	Level        int `json:"level"`
	Health       int `json:"health"`
	Power        int `json:"power"`
	Speed        int `json:"speed"`
}

type ProfilePetSlots struct {
	Slot          int `json:"slot"`
	BattlePetGuid string `json:"battlePetGuid"`
	IsEmpty       bool `json:"isEmpty"`
	IsLocked      bool `json:"isLocked"`
	Abilities     []int `json:"abilites"`
}

type ProfileProgression struct {
	Raids []ProfileProgressionRaid `json:"raids"`
}

type ProfileProgressionRaid struct {
	Name   string `json:"name"`
	Lfr    int `json:"lfr"`
	Normal int `json:"normal"`
	Heroic int `json:"heroic"`
	Mythic int `json:"mythic"`
	Id     int `json:"id"`
	Bosses []struct {
		Id              int `json:"id"`
		Name            string `json:"name"`
		LfrKills        int `json:"lftKills"`
		LftTimestamp    int `json:"lfrTimestamp"`
		NormalKills     int `json:"normalKills"`
		NormalTimestamp int `json:"normalTimestamp"`
		HeroicKills     int `json:"heroicKills"`
		HeroicTimestamp int `json:"heroicTimestamp"`
		MythicKills     int `json:"mythicKills"`
		MythicTimestamp int `json:"mythicTimestamp"`
	}
}

type ProfilePvp struct {
	Brackets ProfilePvpBrackets `json:"brackets"`
}

type ProfilePvpBrackets struct {
	ARENA_BRACKET_2v2          ProfilePvpBracket `json:"ARENA_BRACKET_2v2"`
	ARENA_BRACKET_3v3          ProfilePvpBracket `json:"ARENA_BRACKET_3v3"`
	ARENA_BRACKET_5v5          ProfilePvpBracket `json:"ARENA_BRACKET_5v5"`
	ARENA_BRACKET_RBG          ProfilePvpBracket `json:"ARENA_BRACKET_RBG"`
	ARENA_BRACKET_2v2_SKIRMISH ProfilePvpBracket `json:"ARENA_BRACKET_2v2_SKIRMISH"`
	ARENA_BRACKET_3v3_SKIRMISH ProfilePvpBracket `json:"ARENA_BRACKET_3v3_SKIRMISH"`
	ARENA_BRACKET_5v5_SKIRMISH ProfilePvpBracket `json:"ARENA_BRACKET_5v5_SKIRMISH"`
	ARENA_BRACKET_RBG_SKIRMISH ProfilePvpBracket `json:"ARENA_BRACKET_RBG_SKIRMISH"`


}

type ProfilePvpBracket struct {
	Slug         string `json:"slug"`
	Rating       int `json:"rating"`
	WeeklyPlayed int `json:"weeklyPlayed"`
	WeeklyWon    int `json:"weeklyWon"`
	WeeklyLost   int `json:"weeklyLost"`
	SeasonPlayed int `json:"seasonPlayed"`
	SeasonWon    int `json:"seasonWon"`
	Seasonlost   int `json:"seasonLost"`
}

type ProfileReputation struct {
	Id       int `json:"id"`
	Name     string `json:"name"`
	Standing int `json:"standing"`
	Value    int `json:"value"`
	Max      int `json:"max"`
}

type ProfileStatistics struct {
	Id            int `json:"id"`
	Name          string `json:"name"`
	SubCategories []ProfileStatisticsSubCategories `json:"subCategories"`
}

type ProfileStatisticsSubCategories struct {
	Id          int `json:"id"`
	Name        string `json:"name"`
	Quantity    int `json:"quantity"`
	LastUpdated int `json:"lastUpdated"`
	Money       bool `json:"money"`
	Statistics  []ProfileStatisticsSubCategories `json:"statistics"`
}

type ProfileStats struct {
	Agi                         int     `json:"agi"`
	Armor                       int     `json:"armor"`
	AttackPower                 int     `json:"attackPower"`
	AvoidanceRating             int     `json:"avoidanceRating"`
	AvoidanceRatingBonus        int     `json:"avoidanceRatingBonus"`
	Block                       int     `json:"block"`
	BlockRating                 int     `json:"blockRating"`
	BonusArmor                  int     `json:"bonusArmor"`
	Crit                        float64 `json:"crit"`
	CritRating                  int     `json:"critRating"`
	Dodge                       int     `json:"dodge"`
	DodgeRating                 int     `json:"dodgeRating"`
	Haste                       float64 `json:"haste"`
	HasteRating                 int     `json:"hasteRating"`
	HasteRatingPercent          float64 `json:"hasteRatingPercent"`
	Health                      int     `json:"health"`
	Int                         int     `json:"int"`
	Leech                       int     `json:"leech"`
	LeechRating                 int     `json:"leechRating"`
	LeechRatingBonus            int     `json:"leechRatingBonus"`
	MainHandDmgMax              int     `json:"mainHandDmgMax"`
	MainHandDmgMin              int     `json:"mainHandDmgMin"`
	MainHandDps                 float64 `json:"mainHandDps"`
	MainHandSpeed               float64 `json:"mainHandSpeed"`
	Mana5                       int     `json:"mana5"`
	Mana5Combat                 int     `json:"mana5Combat"`
	Mastery                     int     `json:"mastery"`
	MasteryRating               int     `json:"masteryRating"`
	Multistrike                 float64 `json:"multistrike"`
	MultistrikeRating           int     `json:"multistrikeRating"`
	MultistrikeRatingBonus      float64 `json:"multistrikeRatingBonus"`
	OffHandDmgMax               int     `json:"offHandDmgMax"`
	OffHandDmgMin               int     `json:"offHandDmgMin"`
	OffHandDps                  float64 `json:"offHandDps"`
	OffHandSpeed                float64 `json:"offHandSpeed"`
	Parry                       float64 `json:"parry"`
	ParryRating                 int     `json:"parryRating"`
	Power                       int     `json:"power"`
	PowerType                   string  `json:"powerType"`
	RangedAttackPower           int     `json:"rangedAttackPower"`
	RangedDmgMax                int     `json:"rangedDmgMax"`
	RangedDmgMin                int     `json:"rangedDmgMin"`
	RangedDps                   int     `json:"rangedDps"`
	RangedSpeed                 int     `json:"rangedSpeed"`
	SpeedRating                 int     `json:"speedRating"`
	SpeedRatingBonus            int     `json:"speedRatingBonus"`
	SpellCrit                   float64 `json:"spellCrit"`
	SpellCritRating             int     `json:"spellCritRating"`
	SpellPen                    int     `json:"spellPen"`
	SpellPower                  int     `json:"spellPower"`
	Spr                         int     `json:"spr"`
	Sta                         int     `json:"sta"`
	Str                         int     `json:"str"`
	Versatility                 int     `json:"versatility"`
	VersatilityDamageDoneBonus  float64 `json:"versatilityDamageDoneBonus"`
	VersatilityDamageTakenBonus float64 `json:"versatilityDamageTakenBonus"`
	VersatilityHealingDoneBonus float64 `json:"versatilityHealingDoneBonus"`
}

type ProfileTalents struct {
	Selected   bool `json:"selected"`
	Talents    []struct {
		Tier   int `json:"tier"`
		Column int `json:"column"`
		Spell  struct {
				   Id          int `json:"id"`
				   Name        string `json:"name"`
				   Icon        string `json:"icon"`
				   Description string `json:"description"`
				   CastTime    string `json:"castTime"`
				   CoolDown    string `json:"coolDown"`
			   } `json:"spell"`
	}
	Glyphs     struct {
				   Major []ProfileTalentsGlyphs `json:"major"`
				   Minor []ProfileTalentsGlyphs `json:"minor"`
			   }
	Spec       struct {
				   Name            string `json:"name"`
				   Role            string `json:"role"`
				   BackgroundImage string `json:"backgroundImage"`
				   Icon            string `json:"icon"`
				   Description     string `json:"description"`
				   Order           int `json:"order"`
			   }
	CalcTalent string `json:"calcTalent"`
	CalcSpec   string `json:"calcSpec"`
	CalcGlyph  string `json:"calcGlyph"`
}

type ProfileTalentsGlyphs struct {
	Glyph int `json:"glyph"`
	Item  int `json:"item"`
	Name  string `json:"name"`
	Icon  string `json:"icon"`
}

type ProfileTitles struct {
	Id   int `json:"id"`
	Name string `json:"name"`
}

type ProfileAudit struct {
	AppropriateArmorType         int      `json:"appropriateArmorType"`
	EmptyGlyphSlots              int      `json:"emptyGlyphSlots"`
	EmptySockets                 int      `json:"emptySockets"`
	InappropriateArmorType       struct{} `json:"inappropriateArmorType"`
	ItemsWithEmptySockets        struct{} `json:"itemsWithEmptySockets"`
	LowLevelItems                struct{} `json:"lowLevelItems"`
	LowLevelThreshold            int      `json:"lowLevelThreshold"`
	MissingBlacksmithSockets     struct{} `json:"missingBlacksmithSockets"`
	MissingEnchanterEnchants     struct{} `json:"missingEnchanterEnchants"`
	MissingEngineerEnchants      struct{} `json:"missingEngineerEnchants"`
	MissingExtraSockets          struct {
									 One      int `json:"1"`
									 Two      int `json:"2"`
									 Four     int `json:"4"`
									 Five     int `json:"5"`
									 Six      int `json:"6"`
									 Seven    int `json:"7"`
									 Eight    int `json:"8"`
									 Nine     int `json:"9"`
									 Ten      int `json:"10"`
									 Eleven   int `json:"11"`
									 Twelve   int `json:"12"`
									 Thirteen int `json:"13"`
									 Fourteen int `json:"14"`
									 Fifteen  int `json:"15"`
									 Sixteen  int `json:"16"`
								 } `json:"missingExtraSockets"`
	MissingLeatherworkerEnchants struct{} `json:"missingLeatherworkerEnchants"`
	MissingScribeEnchants        struct{} `json:"missingScribeEnchants"`
	NMissingJewelcrafterGems     int      `json:"nMissingJewelcrafterGems"`
	NoSpec                       bool     `json:"noSpec"`
	NumberOfIssues               int      `json:"numberOfIssues"`
	RecommendedBeltBuckle        struct {
									 Armor                int           `json:"armor"`
									 AvailableContexts    []string      `json:"availableContexts"`
									 BaseArmor            int           `json:"baseArmor"`
									 BonusLists           []interface{} `json:"bonusLists"`
									 BonusStats           []interface{} `json:"bonusStats"`
									 BonusSummary         struct {
															  BonusChances      []interface{} `json:"bonusChances"`
															  ChanceBonusLists  []interface{} `json:"chanceBonusLists"`
															  DefaultBonusLists []interface{} `json:"defaultBonusLists"`
														  } `json:"bonusSummary"`
									 BuyPrice             int    `json:"buyPrice"`
									 ContainerSlots       int    `json:"containerSlots"`
									 Context              string `json:"context"`
									 Description          string `json:"description"`
									 DisplayInfoID        int    `json:"displayInfoId"`
									 Equippable           bool   `json:"equippable"`
									 HasSockets           bool   `json:"hasSockets"`
									 HeroicTooltip        bool   `json:"heroicTooltip"`
									 Icon                 string `json:"icon"`
									 ID                   int    `json:"id"`
									 InventoryType        int    `json:"inventoryType"`
									 IsAuctionable        bool   `json:"isAuctionable"`
									 ItemBind             int    `json:"itemBind"`
									 ItemClass            int    `json:"itemClass"`
									 ItemLevel            int    `json:"itemLevel"`
									 ItemSource           struct {
															  SourceID   int    `json:"sourceId"`
															  SourceType string `json:"sourceType"`
														  } `json:"itemSource"`
									 ItemSpells           []struct {
										 CategoryID int  `json:"categoryId"`
										 Consumable bool `json:"consumable"`
										 NCharges   int  `json:"nCharges"`
										 Spell      struct {
														CastTime    string `json:"castTime"`
														Description string `json:"description"`
														Icon        string `json:"icon"`
														ID          int    `json:"id"`
														Name        string `json:"name"`
													} `json:"spell"`
										 SpellID    int    `json:"spellId"`
										 Trigger    string `json:"trigger"`
									 } `json:"itemSpells"`
									 ItemSubClass         int    `json:"itemSubClass"`
									 MaxCount             int    `json:"maxCount"`
									 MaxDurability        int    `json:"maxDurability"`
									 MinFactionID         int    `json:"minFactionId"`
									 MinReputation        int    `json:"minReputation"`
									 Name                 string `json:"name"`
									 NameDescription      string `json:"nameDescription"`
									 NameDescriptionColor string `json:"nameDescriptionColor"`
									 Quality              int    `json:"quality"`
									 RequiredLevel        int    `json:"requiredLevel"`
									 RequiredSkill        int    `json:"requiredSkill"`
									 RequiredSkillRank    int    `json:"requiredSkillRank"`
									 SellPrice            int    `json:"sellPrice"`
									 Stackable            int    `json:"stackable"`
									 Upgradable           bool   `json:"upgradable"`
								 } `json:"recommendedBeltBuckle"`
	Slots                        struct {
									 One      int `json:"1"`
									 Two      int `json:"2"`
									 Four     int `json:"4"`
									 Five     int `json:"5"`
									 Six      int `json:"6"`
									 Seven    int `json:"7"`
									 Eight    int `json:"8"`
									 Nine     int `json:"9"`
									 Ten      int `json:"10"`
									 Eleven   int `json:"11"`
									 Twelve   int `json:"12"`
									 Thirteen int `json:"13"`
									 Fourteen int `json:"14"`
									 Fifteen  int `json:"15"`
									 Sixteen  int `json:"16"`
								 } `json:"slots"`
	UnenchantedItems             struct {
									 One      int `json:"1"`
									 Two      int `json:"2"`
									 Four     int `json:"4"`
									 Five     int `json:"5"`
									 Six      int `json:"6"`
									 Seven    int `json:"7"`
									 Eight    int `json:"8"`
									 Nine     int `json:"9"`
									 Ten      int `json:"10"`
									 Eleven   int `json:"11"`
									 Twelve   int `json:"12"`
									 Thirteen int `json:"13"`
									 Fourteen int `json:"14"`
									 Fifteen  int `json:"15"`
									 Sixteen  int `json:"16"`
								 } `json:"unenchantedItems"`
	UnspentTalentPoints          int `json:"unspentTalentPoints"`
}