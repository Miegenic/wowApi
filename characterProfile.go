package WowApi

import (
	"io/ioutil"
	"net/http"
	"github.com/pquerna/ffjson/ffjson"
)

func (b *Bnet) GetCharacter(name string, realm string) (CharacterProfile, error) {
	resp, err := b.CreateReq([]string{"character"}, []string{realm, name}, map[string]string{"fields": "achievements,appearance,feed,guild,hunterPets,items,mounts,pets,petSlots,progression,pvp,quests,reputation,statistics,talents,audit"})
	defer resp.Body.Close()
	if err != nil {
		return CharacterProfile{}, err
	}


	characterProfile := CharacterProfile{}

	err = decodeCharacterProfile(&characterProfile, resp)

	return characterProfile, err
}

func (pfeed *ProfileFeed) ParseCriteria() error {

	return nil
}

func decodeCharacterProfile(character *CharacterProfile, resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = ffjson.Unmarshal(body, &character)

	if err != nil {
		return err
	}

	return nil
}
