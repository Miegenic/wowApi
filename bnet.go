package WowApi

import (
	"errors"
	"net/http"
	"time"
)

var SUPPORTED_LANGUAGES = []string{"en_US", "es_MX", "pt_BR", "en_GB", "es_ES", "fr_FR", "ru_RU", "de_DE", "pt_PT", "it_IT", "ko_KR", "zh_TW", "zh_CN"}
var SUPPORTED_REGIONS = []string{"eu", "kr", "tw", "us"}

type Bnet struct {
	Key         string
	Lang        string
	Region      string
	Guild       GuildConfig
	Requests    uint8
	LastRequest time.Time
}

type GuildConfig struct {
	Name  string
	Realm string
}

func (b *Bnet) CheckKey() error {
	if len(b.Key) == 32 {
		return nil
	} else {
		return errors.New("Invalid API Key length")
	}
}

func (b *Bnet) CheckLang() error {
	for _, v := range SUPPORTED_LANGUAGES {
		if v == b.Lang {
			return nil
		}
	}
	return errors.New("Langauge not supported")
}

func (b *Bnet) CheckRegion() error {
	for _, v := range SUPPORTED_REGIONS {
		if v == b.Region {
			return nil
		}
	}
	return errors.New("Region not supported")
}

func (b *Bnet) CreateReq(endpoints []string, ids []string, args map[string]string) (*http.Response, error) {
	url := CreateUrl(*b, endpoints, ids, args)
	res, err := http.Get(url.GenUrl())

	if b.Requests == 0 {
		b.LastRequest = time.Now()
	} else if b.Requests >= 100 {
		return res, errors.New("Requests over the limit (100+ req/sec)")
	}

	if time.Now().Second() > b.LastRequest.Add(time.Second).Second() {
		b.Requests = 0
	} else {
		b.Requests++
	}

	if res.StatusCode == 200 {
		return res, err
	} else if res.StatusCode == 404 {
		return res, errors.New("404 not found")
	} else if res.StatusCode == 403 {
		return res, checkForbiddenError(res)
	} else {
		return res, err
	}
}

func CreateApi(key string, region string, lang string) *Bnet {
	return &Bnet{
		Key:    key,
		Region: region,
		Lang:   lang,
	}
}

func CreateApiGuild(key string, region string, lang string, guild GuildConfig) *Bnet {
	return &Bnet{
		Key:    key,
		Region: region,
		Lang:   lang,
		Guild:  guild,
	}
}

func CreateApiFull(key string, region string, lang string, guildName string, guildRealm string) (*Bnet, *GuildConfig) {
	guild := GuildConfig{
		Name:  guildName,
		Realm: guildRealm,
	}

	return &Bnet{
		Key:    key,
		Region: region,
		Lang:   lang,
		Guild:  guild,
	}, &guild
}
